from lammpscalculator import LAMMPS
from ase.cluster.wulff import wulff_construction
from ase.md.langevin import Langevin
from ase.io import Trajectory
from ase import units

"""
Simple example of parallel usage of the LAMMPS calculator object.

Submit this script to 4 processors.
"""

calc = LAMMPS(processors=(2,2,1),
              pair_style="eam/alloy",
              pair_coeff="* * Au.lammps.eam Au")

atoms = wulff_construction("Au", [(1, 0, 0), (1, 1, 1)], [1.2,1], 10000, 
                           "fcc", rounding="closest", latticeconstant=4.08)
atoms.center(vacuum=10)

atoms.set_calculator(calc)

traj=Trajectory("melting.traj","w",atoms)

dyn = Langevin(atoms, 5 * units.fs, units.kB * 1000, 0.002, logfile="-")

dyn.attach(traj)

dyn.run(100)
