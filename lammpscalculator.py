from lammps import lammps
from ase.calculators.calculator import Calculator
from ase.data import atomic_masses,chemical_symbols
from ase.units import GPa
import ase.parallel as parallel
import numpy as np
import sys
import warnings
import ctypes

TOLERANCE=1e-9

class LAMMPS(Calculator):
    """
    LAMMPS calculator object for ASE

    This calculator requires LAMMPS to be callable from Python. To test this, 
    launch Python interactively and type:
    >>> from lammps import lammps
    >>> import lammps
    
    See the LAMMPS documention for instructions on how build LAMMPS for python:
    http://lammps.sandia.gov/doc/Section_python.html
    
    This calculator is currently tested for pair styles EAM and COMB.

    Parameters:
    
    lammps_session: lammps object
        Use this if a lammps object with custom arguments are needed.
        The default is suitable for most normal use.
    debug: boolean
        Enable command line output, this is mainly useful for debugging.
    comm: MPI4py communicator object
        Use this to split computational resources on multiple 
        simultaneous calculations. This is not needed for parallelizing a single
        calculation. This parameter will be overwritten by a custom lammps object.
    pair_style: str
        Style and arguments for lammps pair_style command. 
        The syntax is described in the lammps documentation.
    pair_coeff: str
        Atom types and arguments for lammps pair_coeff command.
        The syntax is described in the lammps documentation.
    processors: sequence of 3 integers
        Number of processors in each dimension of a 3d grid overlaying
        the simulation domain.
    charge_equilibrate: boolean
        Turn on dynamic charge equilibration. Only available for the COMB
        potential.

    """
    
    default_parameters = {
        "atom_style":"charge",
        "atom_modify":"id yes map array sort 0 0",
        "units":"metal",
        "processors":(1, 1, 1),
        "pair_style":"lj/cut 5",
        "pair_coeff":"1 1 2.0 2.0 5",
        "charge_equilibrate":False}
    
    implemented_properties = ["energy","forces","stress"]

    def __init__(self,lammps_session=None,comm=None,log=None,debug=False,**kwargs):
        Calculator.__init__(self,**kwargs)
        
        if not np.prod(self.parameters["processors"]) == parallel.size:
            raise RuntimeError("Specified processors != physical processors")

        if debug:
            cmdargs=[]
        else:
            cmdargs=["-echo","none","-log","none","-screen","none"]

        if (lammps_session is None) and (comm is None):
            self.lmp=lammps(cmdargs=cmdargs)
        elif lammps_session is None:
            self.lmp=lammps(cmdargs=cmdargs,comm=comm)
        else:
            self.lmp=lammps_session

        if log==None:
            self.log = "none"
        else:
            self.log = str(log)
        
        self.initialized=False

    def calculate(self, atoms, properties, system_changes):
        
        if not self.is_lower_triangular(atoms.get_cell()):
            atoms=self.rotate_cell(atoms)
            warnings.warn("LAMMPS accepts only lower triangular unit cells " +
                          "- the unit cell have been rotated")

        Calculator.calculate(self, atoms, properties, system_changes)
        
        if (("numbers" in system_changes) or ("pbc" in system_changes)):
            self.initialize(atoms)
        elif len(system_changes) != 0:
            self.update(atoms,system_changes)
        
        if "stress" in properties:
            self.lmp.command("thermo_style custom pe pxx")
        else:
            self.lmp.command("thermo_style custom pe")
            
        if self.parameters["charge_equilibrate"]:
            self.lmp.command("run 2")

            charges = self.lmp.gather_atoms("q",1,1)[:len(atoms)]
            atoms.set_initial_charges(charges)
        else:
            self.lmp.command("run 0")
        
        
        natoms = self.lmp.get_natoms()
        # extract energy
        energy=self.lmp.extract_compute("thermo_pe",0,0)
        
        # extract forces
        forces = np.zeros((len(atoms), 3))
        for i in range(3):
            forces[:,i]=self.lmp.gather_atoms("f",1,3)[i:len(atoms)*3:3]

        # extract stresses
        if "stress" in properties:
            stress = np.zeros(6)
            for i,component in enumerate(['pxx', 'pyy', 'pzz', 'pxy', 'pxz', 'pyz']):
                stress[i] = self.lmp.extract_variable(component, None, 0) * -1e-4 * GPa
            self.results["stress"]=stress
        
        self.results["energy"]=energy
        self.results["forces"]=forces
    
    def update(self,atoms,system_changes):

        def lammps_cell(cell):
            x,y,z=cell[0,0],cell[1,1],cell[2,2]
            xy=cell[1,0]*(1.-TOLERANCE)
            xz=cell[2,0]*(1.-TOLERANCE)
            yz=cell[2,1]*(1.-TOLERANCE)
            return x,y,z,xy,xz,yz

        if "cell" in system_changes:
            self.lmp.command("change_box all xy final 0 xz final 0 yz final 0")
            self.lmp.command(("change_box all x final 0 {0} y final 0 {1} z final 0 {2} "+
                              "xy final {3} xz final {4} yz final {5}")\
                             .format(*lammps_cell(atoms.get_cell())))

        if "positions" in system_changes:
            positions = list(atoms.get_positions().ravel())
            c_positions =(ctypes.c_double * len(positions))(*positions)
            self.lmp.scatter_atoms('x', 1, 3, c_positions)
    
    def initialize(self,atoms):
        numbers = atoms.get_atomic_numbers()

        self.number_to_type={}
        self.type_to_number={}
        for i, number in enumerate(np.sort(np.unique(numbers))):
            self.number_to_type[number] = i + 1
            self.type_to_number[i+1] = number

        def convert_pbc(boundary):
            if boundary: return "p"
            else: return "f"
            
        pbc = " ".join([convert_pbc(i) for i in atoms.get_pbc()])
        
        
        self.lmp.command("log {0}".format(self.log))
        self.lmp.command("clear")
        self.lmp.command("echo none")

        self.lmp.command("atom_style {0}".format(self.parameters["atom_style"]))
        self.lmp.command("units {0}".format(self.parameters["units"]))
        self.lmp.command("processors {0}".format(' '.join(map(str,self.parameters["processors"]))))
        self.lmp.command("atom_modify {0}".format(self.parameters["atom_modify"]))
        self.lmp.command("boundary {0}".format(pbc))
        
        self.lmp.command("region cell prism 0 1 0 1 0 1 0 0 0 units box")

        self.lmp.command("create_box {0} cell".format(len(self.number_to_type)))
        
        self.lmp.command("log none")
        for i,atom in enumerate(atoms):
            self.lmp.command("create_atoms {0} single 0 0 0".format(self.number_to_type[atom.number]))

        self.lmp.command("log {0} append".format(self.log))
        
        for key,value in self.number_to_type.iteritems():
            self.lmp.command("mass {0} {1}".format(value,atomic_masses[key]))
        
        self.lmp.command("pair_style {0}".format(self.parameters["pair_style"]))
        self.lmp.command("pair_coeff {0}".format(self.parameters["pair_coeff"]))

        self.lmp.command("neighbor 1.0 bin")
        self.lmp.command("neigh_modify every 1 delay 0 check yes one 8000")

        if self.parameters["charge_equilibrate"]:
            self.lmp.command("fix fix_charge {0}".format(self.parameters["charge_equilibrate"]))

        self.lmp.command("log none")
        for i,atom in enumerate(atoms):
            self.lmp.command("set atom {0} charge {1}".format(i+1,atom.charge))
        self.lmp.command("log {0} append".format(self.log))
        
        self.lmp.command("variable pxx equal pxx")
        self.lmp.command("variable pyy equal pyy")
        self.lmp.command("variable pzz equal pzz")
        self.lmp.command("variable pxy equal pxy")
        self.lmp.command("variable pxz equal pxz")
        self.lmp.command("variable pyz equal pyz")

        self.update(atoms,["cell","positions"])
    
    def group(self,name,atom_ids):
        atom_ids = np.array(atom_ids)+1
        self.lmp.command("group {0} id {1}".format(name," ".join(map(str,atom_ids))))
        
    def langevin(self,time_step,T_start,friction,group_name="all",T_end=None):
        if T_end is None:
            T_end = T_start
        
        self.lmp.command("timestep {0}".format(time_step))
        self.lmp.command("fix fix_nvt {0} nvt temp {1} {2} {3}"\
                         .format(group_name,T_start,T_end,friction))

    def minimize(self,etol=1e-6,ftol=1e-6,maxiter=10000,maxeval=10000):
        self.lmp.command("minimize {0} {1} {2} {3}".format(etol,ftol,maxiter,maxeval))

    def restart(self,file_name,interval=1000):
        self.lmp.command("restart {0} {1}".format(interval,file_name))

    def dump(self,cfg,interval):
        self.lmp.command(("dump dump_run all cfg {0} {1}.*.cfg mass type xsu ysu zsu id " +
                          "vx vy vz fx fy fz proc type q").format(interval,cfg))
        
        numbers = [chemical_symbols[key] for key in sorted(self.number_to_type.keys())]
        self.lmp.command("dump_modify dump_run element {0}".format(" ".join(numbers)))
    
    def thermo(self,quantities=["step","time","temp","pe","etotal"],interval=1,flush=True):
        self.lmp.command("thermo_style custom {0}".format(" ".join(quantities)))
        self.lmp.command("thermo_modify flush {0}".format(self.bool2str(flush)))
        self.lmp.command("thermo {0}".format(interval))

    def run(self,steps,traj=None,interval=1):        
        if traj is None:
            self.lmp.command("run {0}".format(steps))
        else:            
            step_list = [interval]*(steps / interval)+[(steps % interval)]
            traj.write(self.get_lammps_atoms())
            for steps in step_list:
                self.lmp.command("run {0} pre no post no".format(steps))
                traj.write(self.get_lammps_atoms())

    def get_lammps_atoms(self):
        cell=np.zeros((3,3))
        cell[0,0]=self.lmp.extract_global("boxxhi",1)
        cell[1,1]=self.lmp.extract_global("boxyhi",1)
        cell[2,2]=self.lmp.extract_global("boxzhi",1)
        cell[1,0]=self.lmp.extract_global("xy",1)
        cell[2,0]=self.lmp.extract_global("xz",1)
        cell[2,1]=self.lmp.extract_global("yz",1)

        atom_types=self.lmp.gather_atoms("type",0,1)
        numbers=[self.type_to_number[atom_type] for atom_type in atom_types]
        positions=[position for position in self.lmp.gather_atoms("x",1,3)]
        positions=np.reshape(positions,(len(positions)/3,3))
        
        atoms=Atoms(numbers,positions,cell=cell)
        
        charges=self.lmp.gather_atoms("q",1,1)[:len(atoms)]
        atoms.set_initial_charges(charges)

        return atoms

    @staticmethod
    def bool2str(boolean):
        if not isinstance(boolean,bool):
            raise ValueError("Not a boolean.")
        if boolean==True: return "yes"
        else: return "no"

    @staticmethod
    def is_lower_triangular(A):
        def is_zero(x):
            return abs(x) < TOLERANCE
        return is_zero(A[0,1]) and is_zero(A[0,2]) and is_zero(A[1,2])
    
    @classmethod
    def rotate_cell(cls,atoms):
        cell=atoms.get_cell()

        if not cls.is_lower_triangular(cell):
            new_cell = np.zeros((3,3))
            a = cell[0,:]
            b = cell[1,:]
            c = cell[2,:]
            new_cell[0,0] = np.linalg.norm(a)
            a_unit = a/new_cell[0,0]
            axb = np.cross(a,b)
            axb_unit = axb / np.linalg.norm(axb)
            new_cell[1,0] = np.dot(b, a_unit)
            new_cell[1,1] = np.linalg.norm(np.cross(a_unit, b))
            new_cell[2,0] = np.dot(c, a_unit)
            new_cell[2,1] = np.dot(c, np.cross(axb_unit, a_unit))
            new_cell[2,2] = np.linalg.norm(np.dot(c, axb_unit))

            atoms.set_cell(new_cell,scale_atoms=True)
        
        return atoms












