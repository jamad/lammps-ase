LAMMPS calculator for ASE
=============================

This calculator requires LAMMPS to be build as a library. To test this, launch Python and type:

>>> from lammps import lammps
>>> lmp = lammps()

See the LAMMPS documention for instructions on how build LAMMPS for python: http://lammps.sandia.gov/doc/Section_python.html

This calculator is currently tested for pair styles eam, eam/alloy, comb and comb3.